FROM registry.access.redhat.com/ubi8/python-39:1

COPY . .
RUN pip install --no-cache-dir -r requirements.txt
ENTRYPOINT ["bash", "inactive-gitlab-users.sh"]
