#!/usr/bin/python

from __future__ import print_function

import argparse
import datetime
import json
import os
import sys

import gitlab
import psycopg2
import pytz
from dateutil.parser import parse as dateparser
from dateutil.relativedelta import relativedelta

if database_url := os.environ.get("DATABASE_URL"):
    postgres = psycopg2.connect(database_url)
else:
    sys.exit("DATABASE_URL not set")


def timestamp2date(timestamp):
    if timestamp:
        return timestamp.split("T")[0]
    else:
        return None


def is_user_trusted(user):
    trusted_domains = [
        "archlinux.org",
        "canonical.com",
        "debian.org",
        "endlessm.com",
        "fedoraproject.org",
        "gentoo.org",
        "gitlab.gnome.org",
        "gnome.org",
        "igalia.com",
        "kde.org",
        "opensuse.org",
        "puri.sm",
        "redhat.com",
        "suse.com",
        "ubuntu.com",
    ]

    attrs = user.attributes
    identities = [identity["provider"] for identity in attrs["identities"]]

    if "ldapmain" in identities:
        user.customattributes.set("trusted", "true")
        return True

    if "openid_connect" in identities:
        user.customattributes.set("trusted", "true")
        return True

    confirmed_emails = [
        x for x in user.emails.list() if x.confirmed_at if len(x.confirmed_at) > 0
    ]
    if len(confirmed_emails) >= 2:
        user.customattributes.set("trusted", "true")
        return True

    for email in confirmed_emails:
        if email.email.split("@")[1] in trusted_domains:
            user.customattributes.set("trusted", "true")
            return True

    if attrs["two_factor_enabled"]:
        user.customattributes.set("trusted", "true")
        return True

    if user.keys.list() or user.gpgkeys.list():
        user.customattributes.set("trusted", "true")
        return True

    if len(user.identities) >= 2:
        user.customattributes.set("trusted", "true")
        return True

    confirmed_emails = [
        x for x in user.emails.list() if x.confirmed_at if len(x.confirmed_at) > 0
    ]
    if len(confirmed_emails) >= 2:
        user.customattributes.set("trusted", "true")
        return True

    events = user.events.list(all=True)
    for event in events:
        project_id = event.project_id
        project = gl.projects.get(project_id).path_with_namespace
        if project.split("/")[0] in ("GNOME", "Infrastructure", "Teams", "World"):
            user.customattributes.set("trusted", "true")
            return True

    return False


def get_inactive_users(gl, timedelta_unit, timedelta_value):
    fields = [
        "username",
        "email",
        "id",
        "bio",
        "website_url",
        "created_at",
        "current_sign_in_at",
        "last_activity_on",
    ]

    trusted_users = gl.users.list(custom_attributes={"trusted": "true"}, all=True)
    users = gl.users.list(as_list=False, order_by="created_at", sort="asc")
    results = []

    relativedelta_kwargs = {timedelta_unit: int(timedelta_value)}
    timedelta = datetime.datetime.now(pytz.utc) - relativedelta(**relativedelta_kwargs)

    with postgres.cursor() as cursor:
        cursor.execute("select distinct(user_id) from subscriptions")
        subscribed_users = [x[0] for x in cursor.fetchall()]

    for user in users:
        attrs = user.attributes
        userdata = {field: str(attrs[field]) for field in fields}

        if user in trusted_users:
            continue

        if is_user_trusted(user):
            continue

        if user.id in subscribed_users:
            continue

        # Skip user if registered within timedelta
        if dateparser(attrs["created_at"]) > timedelta:
            continue

        created_at = timestamp2date(attrs["created_at"])
        current_sign_in_at = timestamp2date(attrs["current_sign_in_at"])
        last_activity_on = timestamp2date(attrs["last_activity_on"])

        # If user logged in only once or never, check if they made any action.
        if (
            (created_at == current_sign_in_at == last_activity_on)
            or not (current_sign_in_at or last_activity_on)
            or attrs.get("sign_in_count", 0) <= 2
        ):
            events = user.events.list(all=True, lazy=True)
            if len(events) == 0:
                userdata["reason"] = "inactivity"
                results.append(userdata)
                continue
            else:
                user_projects = user.projects.list(all=True)
                skip = False
                if len(user_projects) > 1:
                    empty_repo_counter = 0
                    for proj in user_projects:
                        if proj.attributes.get("forked_from_project"):
                            skip = True
                        proj = gl.projects.get(proj.id)
                        if len(proj.commits.list()) <= 3:
                            empty_repo_counter += 1
                    if empty_repo_counter == len(user_projects):
                        if not skip:
                            userdata["reason"] = "spamrepo"
                            results.append(userdata)

    return results


def delete_snippets_only_users(gl):
    trusted_users = gl.users.list(custom_attributes={"trusted": "true"}, all=True)
    snippets = gl.snippets.public(per_page=100)

    snippets_users = {x.attributes["author"]["id"] for x in snippets}
    for user in snippets_users:
        user = gl.users.get(user)
        if user in trusted_users:
            continue

        if is_user_trusted(user):
            continue

        events = user.events.list(all=True, lazy=True)
        if len(events) == 0:
            delete_user(gl, user.id, "snippets")


def trust_user(gl, user_id):
    user = gl.users.get(user_id, lazy=True)
    user.customattributes.set("trusted", "true")
    print(user_id)


def untrust_user(gl, user_id):
    user = gl.users.get(user_id, lazy=True)
    user.customattributes.delete("trusted")
    print(user_id)


def delete_user(gl, user_id, reason=""):
    try:
        user = gl.users.get(user_id)
        print(f"{user.username},{user.email},{reason}")
        gl.users.delete(user_id)
    except gitlab.exceptions.GitlabDeleteError:
        pass


def block_user(gl, user_id):
    try:
        user = gl.users.get(user_id)
        print(f"{user.username},{user.email},spam")
        user.block()
    except gitlab.exceptions.GitlabBlockError:
        pass


def trust_all_groups(gl):
    groups = gl.groups.list(all=True, visibility="public")
    parent_groups = [grp for grp in groups if not grp.attributes["parent_id"]]
    members = set()

    for group in parent_groups:
        group_members = group.members.all(all=True)
        members.update(group_members)

    for user in members:
        trust_user(gl, user.id)


if __name__ == "__main__":
    GITLAB_TOKEN = os.getenv("GITLAB_TOKEN")
    if GITLAB_TOKEN is None:
        with open("/home/admin/secret/gitlab_rw") as f:
            tokenfile = f.readline()
        GITLAB_TOKEN = tokenfile.rstrip().split("=")[1]

    gl = gitlab.Gitlab(
        "https://gitlab.gnome.org",
        private_token=GITLAB_TOKEN,
        per_page=100,
    )
    gl.auth()

    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest="command")

    trust_groups = subparsers.add_parser(
        "trust-groups", help="mark users which are member of any group as trusted"
    )

    inactive = subparsers.add_parser("get-inactive", help="get inactive users")
    inactive.add_argument(
        "-t",
        "--timedelta",
        default="months:1",
        help="grace period for users to become active (unit:value)",
    )

    trust = subparsers.add_parser("trust", help="mark users as trusted")
    trust.add_argument("user_id", nargs="+", help="user IDs to mark as trusted")

    untrust = subparsers.add_parser("untrust", help="mark users as untrusted")
    untrust.add_argument("user_id", nargs="+", help="user IDs to mark as untrusted")

    delete = subparsers.add_parser("delete", help="delete users")
    delete.add_argument("user_id", nargs="+", help="user IDs to delete")

    delete_from_file = subparsers.add_parser(
        "delete-from-json",
        help="delete users from json file generated with get-inactive",
    )
    delete_from_file.add_argument("filename", help="path to json file")

    snippets = subparsers.add_parser(
        "snippets", help="remove users who have no activity other than posting snippets"
    )

    args = parser.parse_args()

    if args.command == "get-inactive":
        unit, value = args.timedelta.split(":")
        inactive = get_inactive_users(gl, unit, value)
        with open("/tmp/inactive.json", "w") as f:
            json.dump(inactive, f)
    elif args.command == "trust":
        for id in args.user_id:
            trust_user(gl, id)
    elif args.command == "untrust":
        for id in args.user_id:
            untrust_user(gl, id)
    elif args.command == "delete":
        for id in args.user_id:
            delete_user(gl, id)
    elif args.command == "delete-from-json":
        with open(args.filename, "r") as f:
            users = json.load(f)

        for user in users:
            if user["reason"] == "spam":
                block_user(gl, user["id"])
                continue

            delete_user(gl, user["id"], user["reason"])
    elif args.command == "trust-groups":
        trust_all_groups(gl)
    elif args.command == "snippets":
        delete_snippets_only_users(gl)
    else:
        parser.print_help()
