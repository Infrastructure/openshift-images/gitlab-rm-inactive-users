#!/bin/sh
set -e

echo "remove after ${TIMEDELTA_VALUE} ${TIMEDELTA_UNIT}"
echo

python inactive-gitlab-users.py trust-groups >/dev/null
python inactive-gitlab-users.py snippets
python inactive-gitlab-users.py get-inactive -t ${TIMEDELTA_UNIT}:${TIMEDELTA_VALUE} > /tmp/inactive.json
cat /tmp/inactive.json
python inactive-gitlab-users.py delete-from-json /tmp/inactive.json
